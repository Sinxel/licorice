namespace Sinxel.Licorice.Models
{
	public class Token
	{
		public TokenType Type {get; set;}
		public string Value {get; set;}
		public SourceLocation Location {get; set;}

		public Token(SourceLocation location, TokenType type, string value)
		{
			Location =  location;
			Type = type;
			Value = value;
		}

		public override string ToString()
		{
			return "["+Location.Line+":"+Location.Column+"] " + Type + ": " + Value;
			
		}

	}
}