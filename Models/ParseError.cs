namespace Sinxel.Licorice.Models
{
	public class ParseError
	{
		public SourceLocation Location {get; set;}
		public string Message {get; set;}
		public ParseError(SourceLocation location, string message)
		{
			Location = location;
			Message = message;
		}
	}
}