namespace Sinxel.Licorice.Models
{
	public class SourceLocation
	{
		public int Line {get; set;}
		public int Column {get; set;}
		public SourceLocation(int line, int column)
		{
			Line = line;
			Column = column;
		}
		public override string ToString()
		{
			return "["+Line+":"+Column+"]";
		}
	}
}