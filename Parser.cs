using Sinxel.Licorice.Models;
namespace Sinxel.Licorice
{
	public class Parser
	{
		List<Token> tokens;
		private int index = 0;
		
		public Parser(List<Token> tokens)
		{
			this.tokens = tokens;
		}

		public List<Statement> Parse()
		{
			List<Statement> statements = new List<Statement>();
			while(!endScan()){
				
				statements.Add(readStatement());
			}
			return statements;
		}
		Statement readStatement()
		{
			if(match(TokenType.Print)) return readPrintStatement();
			return readExpressionStatement();
		}
		Statement readPrintStatement()
		{
			Expression value = readExpression();
			consume(TokenType.Semicolon, "Expecting a semicolon");
			return new Statement.Print(value);
		}
		Expression readExpression()
		{
			
			return new Expression.Literal(previous().Value);
		}
		Statement readExpressionStatement()
		{
			Expression expression = readExpression();
			
			return new Statement.ExpressionStatement(expression);
		}
		

		bool endScan()
		{
			
			return peek().Type == TokenType.EOF;
		}

		Token peek()
		{
			return tokens[index];
		}

		Token next()
		{
			if(!endScan()) index++;
			return previous();
		}

		Token previous()
		{
			return tokens[index-1];
		}
		private bool match(params TokenType[] types)
		{	
			foreach(TokenType type in types)
			{
				if(check(type))
				{
					next();
					return true;
				}
			}
			return false;
		}
		private bool check(TokenType type)
		{
			if(endScan()) return false;
			return peek().Type == type;
		}
		private Token consume(TokenType type, string message)
		{
			if(check(type)) return next();
			throw new Exception(message+" found: " + peek().Type);
		}

	}
}