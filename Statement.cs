namespace Sinxel.Licorice
{
	public abstract class Statement
	{
		public interface Visitor<T>
		{
			T visitExpressionStatement(ExpressionStatement statement);
			T visitPrintStatement(Print statement);
		}
		
		public class ExpressionStatement : Statement
		{
			public ExpressionStatement(Expression expression)
			{
				this.expression = expression;
			}
			public override T Accept<T>(Visitor<T> visitor)
			{
				return visitor.visitExpressionStatement(this);
			}
			public Expression expression{get; set;}
		}
		public class Print : Statement
		{
			public Print(Expression expression)
			{
				this.expression = expression;
			}
			public override T Accept<T>(Visitor<T> visitor)
			{
				return visitor.visitPrintStatement(this);
			}
			public Expression expression {get; set;}
		}

		public abstract T Accept<T>(Visitor<T> visitor);
	}
}