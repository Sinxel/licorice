﻿using System;
using Sinxel.Licorice.Models;
namespace Sinxel.Licorice
{
    class Program
    {
        public static readonly string Version = "0.1";
        static List<ParseError> errors = new List<ParseError>();
        static void Main(string[] args)
        {
            
			string source = File.ReadAllText("lcr/Examples/parser_tests/Declaring.lcr");
			Scanner scanner = new Scanner(source);
			List<Token> tokens = scanner.Scan();
            Interpreter interpreter = new Interpreter();
            using(StreamWriter writer = new StreamWriter("logs/output.log"))
            {
                foreach(Token token in tokens)
                {
                    writer.WriteLine(token.ToString());
                }
            }
            if(errors.Count>0){
                Console.WriteLine("source code contains: " + errors.Count + " errors, please fix them and try again.");
                Environment.Exit(-1);
            }
            Parser parser = new Parser(tokens);
            List<Statement> statements = parser.Parse();
            interpreter.interpret(statements);
        }

        public static void Error(ParseError error)
        {
            Console.WriteLine("Parse Error at: " + error.Location.ToString() + ", " + error.Message);
            errors.Add(error);
        }
    }
}
