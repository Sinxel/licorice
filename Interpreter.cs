namespace Sinxel.Licorice
{
	public class Interpreter : Expression.Visitor<object>, Statement.Visitor<string>
	{
		public void interpret(List<Statement> statements)
		{
			foreach(Statement statement in statements)
			{
				Console.WriteLine(statement);
				Execute(statement);
			}
		}
		private void Execute(Statement statement)
		{
			statement.Accept(this);
		}
		private object Evaluate(Expression expression)
		{
			return expression.Accept(this);
		}

		public object visitLiterlaExpression(Expression.Literal expression)
		{
			
			return expression.Value;
		}
		public string visitPrintStatement(Statement.Print statement)
		{
			object value = Evaluate(statement.expression);
			Console.WriteLine(value);
			return null;
		}
		public string visitExpressionStatement(Statement.ExpressionStatement statement)
		{
			Evaluate(statement.expression);
			return null;
		}

	}  
}