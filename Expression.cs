using Sinxel.Licorice.Models;
namespace Sinxel.Licorice
{
	public abstract class Expression
	{
		public interface Visitor<T>
		{
			
			T visitLiterlaExpression(Literal expression);
		}
		public class Literal : Expression
		{
			public Literal(string value)
			{
				Value = value;
			}
			public override T Accept<T>(Visitor<T> visitor)
			{
				return visitor.visitLiterlaExpression(this);
			}
			public string Value{get; set;}
			
		}
		

		public abstract T Accept<T>(Visitor<T> visitor);




		
	}

}