using Sinxel.Licorice.Models;

namespace Sinxel.Licorice
{
	class Scanner
	{
		string source;
		List<Token> tokens = new List<Token>();
		int index = 0;
		int line = 1;
		int start = 0;
		int column = 0;
		readonly string escapables = "\\snbftr'\"$";
		string hex = "abcdef";
		Dictionary<string, TokenType> keywords = new Dictionary<string, TokenType>(){
			{"return", TokenType.Return},
			{"true", TokenType.True},
			{"false", TokenType.False},
			{"int", TokenType.DeclareInt},
			{"string", TokenType.DeclareString},
			{"print", TokenType.Print},

		};

		public Scanner(string source)
		{
			this.source = source;
		}

		public List<Token> Scan()
		{
			while (!endScan())
			{
				start = index;
				readToken();
			}
			addToken(new SourceLocation(line, ++column), TokenType.EOF, "");

			return tokens;
		}

		void readToken()
		{
			char c = next();

			switch (c)
			{
				case '/':
					readSlash(); break;
				case '"':
					readString(); break;
				case '\'':
					readChar(); break;
				case '(':
					addToken(TokenType.LeftParentheses); break;
				case ')':
					addToken(TokenType.RightParentheses); break;
				case '.':
					readDot(); break;
				case ';':
					addToken(TokenType.Semicolon); break;
				case '{':
					addToken(TokenType.LeftBrace); break;
				case '}':
					addToken(TokenType.RightBrace); break;
				case '[':
					addToken(TokenType.LeftBracket); break;
				case ']':
					addToken(TokenType.RightBracket); break;
				case ':':
					addToken(TokenType.Colon); break;
				case '&':
					addToken(match('&') ? TokenType.LogicalAnd : TokenType.And); break;
				case '=':
					addToken(match('=') ? TokenType.EqualEqual : TokenType.Equal); break;
				case '|':
					addToken(match('|') ? TokenType.LogicalOr : TokenType.Or); break;
				case '!':
					addToken(match('=') ? TokenType.BangEqual : TokenType.Bang); break;
				case '<':
					readLess(); break;
				case '>':
					readGreater(); break;
				case '\n':
					addLine(); break;
				case '+':
					readPlus(); break;
				case '-':
					readMinus(); break;
				case '*':
					addToken(match('=')? TokenType.StarEqual:TokenType.Star); break;
				case ' ':
				case '\t':
				case '\r':
					//nothing
					break;
				default:
					if (isNumeric(c))
					{
						readNumber();
					}
					else
					{
						readIdentifier();
					}
					break;
			}

		}
		void readLess()
		{
			if(peek()=='='){
				next();
				addToken(TokenType.LessOrEqual);
			}else if(peek()=='<')
			{
				next();
				addToken(TokenType.LeftShift);
			}else{
				addToken(TokenType.Less);
			}
		}
		void readGreater()
		{
			if(peek()=='='){
				next();
				addToken(TokenType.GreaterOrEqual);
			}else if(peek()=='>')
			{
				next();
				addToken(TokenType.RightShift);
			}else{
				addToken(TokenType.Greater);
			}
		}
		void readMinus()
		{
			if(peek()=='='){
				next();
				addToken(TokenType.MinusEqual);
			}else if(peek()=='-')
			{
				next();
				addToken(TokenType.Decrement);
			}else{
				addToken(TokenType.Minus);
			}
		}
		void readPlus()
		{
			if(peek()=='='){
				next();
				addToken(TokenType.PlusEqual);
			}else if(peek()=='+')
			{
				next();
				addToken(TokenType.Increment);
			}else{
				addToken(TokenType.Plus);
			}
		}
		bool isNumeric(char c)
		{
			return Char.IsDigit(c);
		}
		void readDot()
		{
			if (Char.IsDigit(peek()))
			{
				readNumber();
			}
			else
			{
				addToken(TokenType.Dot);
			}

		}

		void readNumber()
		{
			SourceLocation location = new SourceLocation(line, column);
			string value = previous().ToString();
			while (isInNumber(peek(), value))
			{
				value += next().ToString();
				if (previous() == 'f' && value.IndexOf(".") != -1) break;
			}
			addToken(location, TokenType.Number, value);
		}
		bool isInNumber(char c, string value)
		{

			if (Char.IsDigit(c)) return true;
			if (c == 'f' && value.IndexOf(".") != -1) return true;
			if (c == '.' && value.IndexOf(".") == -1) return true;
			string cs = c.ToString().ToLower();
			if (cs == "x" && value.Length == 1) return true;
			if (hex.IndexOf(cs) != -1 && value.Length > 1 && value.Substring(0, 2).ToLower() == "0x") return true;
			return false;
		}

		void readSlash()
		{

			if (match('/'))
			{
				skipSingleLineComment();
			}
			else if (match('*'))
			{
				skipMultiLinesComment();
			}
			else
			{
				addToken(TokenType.Slash);
			}

		}
		void skipSingleLineComment()
		{
			while (peek() != '\n' && !endScan()) next();

		}
		void skipMultiLinesComment()
		{
			while (!endScan())
			{
				char c = next();
				if (c == '*' && match('/')) return;
				if (c == '\n')
				{
					addLine();
				}
			}
			Program.Error(new ParseError(new SourceLocation(line, column), "Unterminated multilines comment"));
		}
		char previous()
		{
			return source[index - 1];
		}
		void addLine()
		{
			line++;
			column = 0;
		}
		bool match(char c)
		{
			if (endScan()) return false;
			if (peek() != c) return false;
			column++;
			index++;
			return true;
		}
		void readChar()
		{

			SourceLocation location = new SourceLocation(line, column);
			char p = peek();
			if (p == '\\')
			{
				next();
				if (escapables.IndexOf(peek()) != -1)
				{
					next();
				}
				else
				{
					p = peek();
					if (p == 'u' || p == 'U')
					{
						readUnicode();
					}
					else
					{
						Program.Error(new ParseError(new SourceLocation(line, ++column), "Unrecognized escape sequence: \\" + p));
					}

				}
			}
			else
			{
				next();
			}
			if (match('\''))
			{
				next();
			}
			else
			{
				Program.Error(new ParseError(new SourceLocation(line, ++column), "Unterminated character"));
			}
			addToken(TokenType.Character);


		}
		void readString()
		{

			SourceLocation location = new SourceLocation(line, column);
			while (peek() != '"' && !endScan())
			{

				if (peek() == '\n')
				{
					addLine();
				}
				if (peek() == '\\')
				{

					next();
					string p = peek().ToString();

					if (escapables.IndexOf(p) != -1)
					{

						next();

					}
					else
					{
						if (p.ToLower() == "u")
						{
							readUnicode();
						}
						else
						{
							Program.Error(new ParseError(new SourceLocation(line, ++column), "Unrecognized escape sequence: \\" + p));
						}
					}

				}
				else
				{
					next();
				}


			}
			if (endScan())
			{
				Program.Error(new ParseError(new SourceLocation(line, ++column), "Unterminated string"));
				return;
			}
			next();
			addToken(location, TokenType.String);
		}
		void readUnicode()
		{
			next();
			for (int i = 0; i < 4; i++)
			{
				if (endScan())
				{
					Program.Error(new ParseError(new SourceLocation(line, ++column), "Unterminated character"));
					return;
				}
				string p = peek().ToString().ToLower();
				if (hex.IndexOf(p) == -1 && !Char.IsDigit(p[0]))
				{
					Program.Error(new ParseError(new SourceLocation(line, ++column), "Invalid unicode value: " + source.Substring(start, index - start + 1)));
					return;
				}
				next();
			}
		}
		void readIdentifier()
		{
			SourceLocation location = new SourceLocation(line, column);
			while (isValidIdentifierChar(peek())) next();
			string value = source.Substring(start, index-start);
			TokenType type = TokenType.Identifier;
			if(keywords.ContainsKey(value))
			{
				type = keywords[value];
			}
			
			addToken(location, type);

		}
		void addToken(TokenType type)
		{
			SourceLocation location = new SourceLocation(line, column);
			addToken(location, type);
		}
		void addToken(SourceLocation location, TokenType type, string value)
		{

			Token token = new Token(location, type, value);
			tokens.Add(token);
		}
		void addToken(SourceLocation location, TokenType type)
		{
			string value = source.Substring(start, index - start);
			Token token = new Token(location, type, value);
			tokens.Add(token);
		}
		bool isValidIdentifierChar(char c)
		{
			return Char.IsLetter(c) || Char.IsDigit(c) || c == '_';
		}
		bool endScan()
		{
			return index >= source.Length;
		}
		char next()
		{
			column++;
			return source[index++];
		}

		char peek()
		{
			if (endScan()) return '\0';
			return source[index];
		}




	}
}